<!DOCTYPE html>

<html lang="en">

<head>



    <meta charset="utf-8">

    <title>Ar Plus Refrigeração e Climatização - Joinville - Santa Catarina - Brasil</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <link rel="shortcut icon" href="images/favicon.ico">



    <!-- CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="css/flexslider.css" rel="stylesheet" type="text/css" />

    <link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />

    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all" />

    <link href="css/owl.carousel.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet" type="text/css" />



    <!-- FONTS -->

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500italic,700,500,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">



    <!-- SCRIPTS -->

    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <!--[if IE]><html class="ie" lang="en"> <![endif]-->



    <script src="js/jquery.min.js" type="text/javascript"></script>

    <script src="js/bootstrap.min.js" type="text/javascript"></script>



    <!--Angularjs-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-sanitize.min.js"></script>

    <!--end Angularjs-->





    <script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>

    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>

    <script src="js/superfish.min.js" type="text/javascript"></script>

    <script src="js/jquery.flexslider-min.js" type="text/javascript"></script>

    <script src="js/owl.carousel.js" type="text/javascript"></script>

    <script src="js/jquery.mb.YTPlayer.js" type="text/javascript"></script>

    <script src="js/animate.js" type="text/javascript"></script>

    <script src="js/jquery.BlackAndWhite.js"></script>

    <script src="js/myscript.js" type="text/javascript"></script>

    <script src="js/angular/home.js" type="text/javascript"></script>



    <script>



        //PrettyPhoto

        jQuery(document).ready(function() {

            $("a[rel^='prettyPhoto']").prettyPhoto();

        });



        //BlackAndWhite

        $(window).load(function(){

            $('.client_img').BlackAndWhite({

                hoverEffect : true, // default true

                // set the path to BnWWorker.js for a superfast implementation

                webworkerPath : false,

                // for the images with a fluid width and height

                responsive:true,

                // to invert the hover effect

                invertHoverEffect: false,

                // this option works only on the modern browsers ( on IE lower than 9 it remains always 1)

                intensity:1,

                speed: { //this property could also be just speed: value for both fadeIn and fadeOut

                    fadeIn: 300, // 200ms for fadeIn animations

                    fadeOut: 300 // 800ms for fadeOut animations

                },

                onImageReady:function(img) {

                    // this callback gets executed anytime an image is converted

                }

            });

        });



    </script>



</head>

    <body>



        <!-- PRELOADER -->

        <img id="preloader" src="images/preloader.gif" alt="" />

        <!-- //PRELOADER -->

        <div class="preloader_hide" ng-app="AppAngular" ng-controller="homeController">



            <!-- PAGE -->

            <div id="page">




                <?php require_once('menu.php'); ?>



                <!-- HOME -->

                <section id="home" class="padbot0">



                    <!-- TOP SLIDER -->

                    <div class="flexslider top_slider">

                        <ul class="slides">

                            <li class="slide1">

                                <div class="flex_caption1">

                                    <p class="title1 captionDelay1 FromBottom">Planos de </p>

                                    <p class="title2 captionDelay2 FromBottom"> Manutenção</p>

                                    <p class="title4 captionDelay5 FromBottom">Conheça nossos planos de economia e tranquilidade para seu negócio.</p>

                                </div>

                                <a class="slide_btn FromRight" href="#contacts" >Consulte</a>

                            <li class="slide2">

                                <div class="flex_caption1">

                                    <p class="title1 captionDelay6 FromLeft">Climatização <o></o></p>

                                    <p class="title2 captionDelay4 FromLeft">Residencial</p>

                                    <p class="title4 captionDelay7 FromLeft">Sua casa com muito mais conforto, tecnologia e saúde.</p>

                                </div>

                                <a class="slide_btn FromRight" href="#contacts" >Consulte</a>

                            </li>

                            <li class="slide3">

                                <div class="flex_caption1">

                                    <p class="title1 captionDelay2 FromTop">Climatização</p>

                                    <p class="title3 captionDelay6 FromTop">Industrial</p>

                                    <p class="title4 captionDelay7 FromBottom">Soluções inteligentes com economia e alta performance para sua industria.</p>

                                </div>

                                <a class="slide_btn FromRight" href="#contacts" >Consulte</a>

                            </li>

                        </ul>





                        <!-- VIDEO BACKGROUND -->

                      <a name="P2" class="player" id="P2" data-property="{videoURL:'_MV9cUCIKjs',containment:'.top_slider',autoPlay:true, mute:true, startAt:0, opacity:1}"></a>

                      <!-- //VIDEO BACKGROUND -->

                    </div><!-- //TOP SLIDER -->

                </section><!-- //HOME -->





                <!-- ABOUT -->

                <section id="about">



                    <!-- SERVICES -->

                    <div class="services_block padbot40" data-appear-top-offset="-200" data-animated="fadeInUp">



                        <!-- CONTAINER -->

                        <div class="container">



                            <!-- ROW -->

                            <div class="row">

                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ss-12 margbot30">

                                    <a class="services_item" href="javascript:void(0);" >

                                        <p><b>Planos</b> de Manutenção</p>

                                        <span>Corretiva e preventiva, comodidade e satisfação.</span>

                                    </a>

                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ss-12 margbot30">

                                    <a class="services_item" href="javascript:void(0);" >

                                        <p><b>Projetos</b> Residenciais</p>

                                        <span>Climatização desenvolvida para maior economia e conforto.</span>

                                    </a>

                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ss-12 margbot30">

                                    <a class="services_item" href="javascript:void(0);" >

                                        <p><b>Climatização</b> Empresarial</p>

                                        <span>Conforto, segurança e qualidade do ar no seu negócio.</span>

                                    </a>

                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ss-12 margbot30">

                                    <a class="services_item" href="javascript:void(0);" >

                                        <p><b>Projetos</b> Industriais</p>

                                        <span>Planejamento, fabricação e instalação para industrias.</span>

                                    </a>

                                </div>

                            </div><!-- //ROW -->

                        </div><!-- //CONTAINER -->

                    </div><!-- //SERVICES -->



                    <!-- CLEAN CODE -->

                    <div class="cleancode_block">



                        <!-- CONTAINER -->

                        <div class="container" data-appear-top-offset="-200" data-animated="fadeInUp">



                            <!-- CASTOM TAB -->

                            <div id="myTabContent" class="tab-content">

                                <div class="tab-pane fade in active clearfix" id="tab1">

                                    <p class="title"><b>Nossos</b> Serviços</p>

                                    <span>Nossa empresa conta com uma equipe altamente qualificada e comprometida em executar com excelência tudo o que produzimos.</span>

                                </div>

                                <div class="tab-pane fade clearfix" id="tab2">

                                    <p class="title"><b>Diferencias</b> & Qualidades</p>

                                    <span>A Arplus desenvolve produto específico que proporciona a redução nos custos com ferramentas, aumenta a produtividade e elimina o possível retrabalho.</span>

                                </div>

                                <div class="tab-pane fade clearfix" id="tab3">

                                    <p class="title"><b>Manutenção</b> Preventiva</p>

                                    <span>A Manutenção Preventiva é uma programação pertinente às ações de manutenção projetadas pelo responsável técnico ao elaborar o PMOC (Plano de Manutenção, Operação e Controle). De acordo com  portaria GM/MS n 3523 de 28/08/1998.</span>

                                </div>

                                <div class="tab-pane fade clearfix" id="tab4">

                                    <p class="title"><b>Manutenção</b> Corretiva</p>

                                    <span>Manutenção Corretiva é aquela realizada após a ocorrência de uma falha e visa restaurar a capacidade produtiva do equipamento. A Arplus conta com mecânicos qualificados que possibilitam o diagnóstico correto e a excelência dos serviços prestados.</span>

                                </div>

                                <div class="tab-pane fade clearfix" id="tab5">

                                    <p class="title"><b>Calderaria</b> Leve</p>

                                    <span> A Arplus possui capacidade técnica e fabril para atender os projetos como: dutos de ar em chapa de aço, estruturas metálicas e plataformas. A Caldeiraria Leve Interna permite o acompanhamento dos projetos pela engenharia e a redução do custo da obra, pois exclui a necessidade de contratação de terceiros.</span>

                                </div>

                                <div class="tab-pane fade clearfix" id="tab6">

                                    <p class="title"><b>Infraestrutura</b> & Instalação</p>

                                    <span>A Arplus mantém parceria com profissionais de arquitetura, designers interiores, fabricantes de Condicionadores de Ar e tem sempre a solução ideal para o seu ambiente.



                                    Nossa engenharia proporciona as sugestões dos produtos, bem como auxilia no desenvolvimento da área técnica.

                                    </span>

                                </div>

                            </div>

                            <div id="myTab">

                                <ul class="nav nav-tabs">

                                    <li class="active">

                                        <a href="#tab1" class="i1" data-toggle="tab" ><i></i><span>CLIQUE E VEJA</span></a>

                                    </li>

                                    <li><a href="#tab2" class="i2" data-toggle="tab" ><i></i><span>DIFERENCIAIS</span></a></li>

                                    <li><a href="#tab3" class="i3" data-toggle="tab" ><i></i><span>PREVENTIVA</span></a></li>

                                    <li><a href="#tab4" class="i4" data-toggle="tab" ><i></i><span>CORRETIVA</span></a></li>

                                    <li><a href="#tab5" class="i5" data-toggle="tab" ><i></i><span>CALDERARIA</span></a></li>

                                    <li><a href="#tab6" class="i6" data-toggle="tab" ><i></i><span>INSTALAÇÃO</span></a></li>

                                </ul><!-- CASTOM TAB -->

                            </div>

                        </div><!-- //CONTAINER -->

                    </div><!-- //CLEAN CODE -->



                    <!-- MULTI PURPOSE -->

                    <div class="purpose_block">



                        <!-- CONTAINER -->

                        <div class="container">



                            <!-- ROW -->

                            <div class="row">



                                <div class="col-lg-7 col-md-7 col-sm-7" data-appear-top-offset="-200" data-animated="fadeInLeft">

                                    <h2><b>Empresa Focada</b> na Excelência</h2>

                                    <p>Iniciamos as atividades em 2005 com o objetivo de oferecer soluções inteligentes para um mercado cada vez mais exigente e que aspirava por excelência de produtos e serviços.</p>

                                    <p>Nossa empresa conta com uma equipe altamente qualificada e comprometida em executar com excelência tudo o que produzimos.</p>

                                    <a class="btn btn-active" href="#contacts" ><span data-hover="Clique aqui">Entre em Contato</span></a>

                                    <a class="btn" href="blog.html">Visite nosso Blog</a>

                                </div>



                                <div class="col-lg-5 col-md-5 col-sm-5 ipad_img_in" data-appear-top-offset="-200" data-animated="fadeInRight">

                                    <img class="ipad_img1" src="images/img1.png" alt="" />

                                </div>

                            </div><!-- //ROW -->

                        </div><!-- //CONTAINER -->

                    </div><!-- //MULTI PURPOSE -->

                </section><!-- //ABOUT -->





                <!-- PROJECTS -->

                <section id="projects" class="padbot20">



                    <!-- CONTAINER -->

                    <div class="container">

                        <h2><b>Projetos</b> Executados</h2>

                    </div><!-- //CONTAINER -->





                    <div class="projects-wrapper" data-appear-top-offset="-200" data-animated="fadeInUp">

                        <!-- PROJECTS SLIDER -->

                        <div class="owl-demo owl-carousel projects_slider">



                            <!-- work1 -->

                            <div class="item">

                                <div class="work_item">

                                    <div class="work_img">

                                        <img src="images/works/1.jpg" alt="" />

                                        <a class="zoom" href="images/works/1.jpg" rel="prettyPhoto[portfolio1]" ></a>

                                    </div>

                                    <div class="work_description">

                                        <div class="work_descr_cont">

                                            <a href="portfolio-post.html" >Clique para ver!</a>

                                            <span>Visite nosso Blog</span>

                                        </div>

                                    </div>

                                </div>

                            </div><!-- //work1 -->



                            <!-- work2 -->

                            <div class="item">

                                <div class="work_item">

                                    <div class="work_img">

                                        <img src="images/works/2.jpg" alt="" />

                                        <a class="zoom" href="images/works/2.jpg" rel="prettyPhoto[portfolio1]" ></a>

                                    </div>

                                    <div class="work_description">

                                        <div class="work_descr_cont">

                                            <a href="portfolio-post.html" >Clique para ver!</a>

                                            <span>Visite nosso Blog</span>

                                        </div>

                                    </div>

                                </div>

                            </div><!-- //work2 -->



                            <!-- work3 -->

                            <div class="item">

                                <div class="work_item">

                                    <div class="work_img">

                                        <img src="images/works/3.jpg" alt="" />

                                        <a class="zoom" href="images/works/3.jpg" rel="prettyPhoto[portfolio1]" ></a>

                                    </div>

                                    <div class="work_description">

                                        <div class="work_descr_cont">

                                        <a href="portfolio-post.html" >Clique para ver!</a>

                                            <span>Visite nosso Blog</span>

                                        </div>

                                    </div>

                                </div>

                            </div><!-- //work3 -->



                            <!-- work4 -->

                            <div class="item">

                                <div class="work_item">

                                    <div class="work_img">

                                        <img src="images/works/4.jpg" alt="" />

                                        <a class="zoom" href="images/works/4.jpg" rel="prettyPhoto[portfolio1]" ></a>

                                    </div>

                                    <div class="work_description">

                                        <div class="work_descr_cont">

                                            <a href="portfolio-post.html" >Clique para ver!</a>

                                            <span>Visite nosso Blog</span>

                                        </div>

                                    </div>

                                </div>

                            </div><!-- //work4 -->



                                <!-- work5 -->

                            <div class="item">

                                <div class="work_item">

                                    <div class="work_img">

                                        <img src="images/works/5.jpg" alt="" />

                                        <a class="zoom" href="images/works/5.jpg" rel="prettyPhoto[portfolio1]" ></a>

                                    </div>

                                    <div class="work_description">

                                        <div class="work_descr_cont">

                                            <a href="portfolio-post.html" >Clique para ver!</a>

                                            <span>Visite nosso Blog</span>

                                        </div>

                                    </div>

                                </div>

                            </div><!-- //work5 -->



                                <!-- work6 -->

                            <div class="item">

                                <div class="work_item">

                                    <div class="work_img">

                                        <img src="images/works/6.jpg" alt="" />

                                        <a class="zoom" href="images/works/6.jpg" rel="prettyPhoto[portfolio1]" ></a>

                                    </div>

                                    <div class="work_description">

                                        <div class="work_descr_cont">

                                            <a href="portfolio-post.html" >CVale</a>

                                            <span>15 Abril, 2016</span>

                                        </div>

                                    </div>

                                </div>

                            </div><!-- //work6 -->



                                    <!-- work7 -->

                            <div class="item">

                                <div class="work_item">

                                    <div class="work_img">

                                        <img src="images/works/7.jpg" alt="" />

                                        <a class="zoom" href="images/works/7.jpg" rel="prettyPhoto[portfolio1]" ></a>

                                    </div>

                                    <div class="work_description">

                                        <div class="work_descr_cont">

                                            <a href="portfolio-post.html" >Colgate</a>

                                            <span>15 Abril, 2016</span>

                                        </div>

                                    </div>

                                </div>

                            </div><!-- //work7 -->



                        </div><!-- //PROJECTS SLIDER -->

                    </div>



                <span ng-init="carregarUltimos()"/>

                <!-- NEWS -->

                <section id="news">



                    <!-- CONTAINER -->

                    <div class="container">

                        <h2><b>Com a palavra, </b> o cliente!</h2>



                        <!-- TESTIMONIALS -->

                        <div class="testimonials" data-appear-top-offset="-200" data-animated="fadeInUp">



                            <!-- TESTIMONIALS SLIDER -->

                            <div class="owl-demo owl-carousel testim_slider">



                                <!-- TESTIMONIAL1 -->

                                <div class="item">

                                    <div class="testim_content">“Pellentesque porttitor purus sit amet enim aliquet auctor. Quisque pellentesque dui in lobortis egestas. Nullam fringilla eu sem maximus mollis. Maecenas et odio semper, commodo elit sed, porta orci. Etiam gravida mauris sed iaculis tristique. Mauris non nunc quis nulla dignissim tempus et sit amet lorem. Integer ultrices justo purus, a finibus turpis fermentum vitae.”</div>

                                    <div class="testim_author">—  Anna Balashova, <b>CVale</b></div>

                                </div><!-- TESTIMONIAL1 -->



                                <!-- TESTIMONIAL2 -->

                                <div class="item">

                                    <div class="testim_content">“Pellentesque porttitor purus sit amet enim aliquet auctor. Quisque pellentesque dui in lobortis egestas. Nullam fringilla eu sem maximus mollis. Maecenas et odio semper, commodo elit sed, porta orci. Etiam gravida mauris sed iaculis tristique. Mauris non nunc quis nulla dignissim tempus et sit amet lorem. Integer ultrices justo purus, a finibus turpis fermentum vitae.”</div>

                                    <div class="testim_author">—  John Dave, <b>Marcopolo</b></div>

                                </div><!-- TESTIMONIAL2 -->



                                <!-- TESTIMONIAL3 -->

                                <div class="item">

                                    <div class="testim_content">“Pellentesque porttitor purus sit amet enim aliquet auctor. Quisque pellentesque dui in lobortis egestas. Nullam fringilla eu sem maximus mollis. Maecenas et odio semper, commodo elit sed, porta orci. Etiam gravida mauris sed iaculis tristique. Mauris non nunc quis nulla dignissim tempus et sit amet lorem. Integer ultrices justo purus, a finibus turpis fermentum vitae.”</div>

                                    <div class="testim_author">—  Paulo Monteiro, <b>Tupy</b></div>

                                </div><!-- TESTIMONIAL3 -->

                            </div><!-- TESTIMONIALS SLIDER -->

                        </div><!-- //TESTIMONIALS -->



                        <!-- RECENT POSTS -->

                        <div class="row recent_posts" data-appear-top-offset="-200" data-animated="fadeInUp">

                            <div

                                class="col-lg-4 col-md-4 col-sm-4 padbot30 post_item_block"

                                ng-repeat="(key, value) in arrUltimosPosts">

                                <div class="post_item">

                                    <div class="post_item_img">

                                        <img

                                            src="images/blog/1.jpg"

                                            alt=""

                                            ng-src="{{value.thumbnail_images.full.url}}"

                                            />

                                        <a

                                            ng-href="value.url"

                                            href="blog.html"



                                         >









                                        </a>

                                    </div>

                                    <div class="post_item_content">

                                        <a

                                            class="title"

                                            ng-href="{{value.url}}"

                                            href="blog.html"

                                            >{{value.title}}.

                                        </a>

                                        <ul class="post_item_inf">

                                            <li>

                                                <a href="javascript:void(0);" >{{value.author.name}}</a> |

                                            </li>

                                            <li>

                                                <a

                                                    href="javascript:void(0);"

                                                    ng-repeat="(key, value) in value.categories" >Contratos e Serviços</a> |

                                            </li>

                                            <li><a href="javascript:void(0);" >10 Comentários</a></li>

                                        </ul>

                                    </div>

                                </div>

                            </div>



                        </div><!-- RECENT POSTS -->

                    </div><!-- //CONTAINER -->

                </section><!-- //NEWS -->





                <section>

                    <div id="maps">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14303.453598818096!2d-48.8490338!3d-26.3309224!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa17a45f796dabc80!2sARPLUS!5e0!3m2!1spt-BR!2sbr!4v1497660696233" ></iframe>

                    </div>

                </section>

            </div><!-- //PAGE -->



            <!-- CONTACTS -->

            <section id="contacts">

            </section>

            <!-- //CONTACTS -->



















            <!-- FOOTER -->

            <footer>



                <!-- CONTAINER -->

                <div class="container">



                    <!-- ROW -->

                    <div class="row" data-appear-top-offset="-200" data-animated="fadeInUp">



                        <div class="col-lg-4 col-md-4 col-sm-6 padbot30">

                            <h4><b>Posts</b> recentes</h4>

                            <div

                                class="recent_posts_small clearfix"

                                ng-repeat="(key, value) in arrUltimosPosts">



                                <div class="post_item_img_small">

                                    <img

                                        src="images/blog/1.jpg"

                                        alt=""

                                        ng-src="{{value.thumbnail}}" />

                                </div>

                                <div class="post_item_content_small">

                                    <a

                                        class="title"

                                        ng-href="<?php echo $current_url;?>/site/blog.php#!post/{{value.id}}"

                                        href="blog.html"

                                        ng-bind-html="value.title"

                                        >



                                    </a>

                                    <ul class="post_item_inf_small">

                                        <li>{{value.date}}</li>

                                    </ul>

                                </div>

                            </div>





                        </div>



                        <div class="col-lg-4 col-md-4 col-sm-6 padbot30 foot_about_block">

                            <h4><b>Sobre</b> nós</h4>

                            <p>Iniciamos as atividades em 2005 com o objetivo de oferecer soluções inteligentes para um mercado cada vez mais exigente e que aspirava por excelência de produtos e serviços. Nossa empresa conta com uma equipe altamente qualificada e comprometida em executar com excelência tudo o que produzimos.</p>

                            <ul class="social">

                                <li><a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a></li>

                                <li><a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>

                                <li><a href="javascript:void(0);" ><i class="fa fa-youtube"></i></a></li>

                                <li><a href="javascript:void(0);" ><i class="map_show fa fa-map-marker"></i></a></li>

                            </ul>

                        </div>



                        <div class="respond_clear"></div>



                        <div class="col-lg-4 col-md-4 padbot30">

                            <h4><b>Fale</b> Conosco</h4>



                            <!-- CONTACT FORM -->

                            <div class="span9 contact_form">

                                <div id="note"></div>

                                <div id="fields">

                                    <form id="contact-form-face" class="clearfix" action="#">

                                        <input type="text" name="name" value="Nome" onFocus="if (this.value == 'Name') this.value = '';" onBlur="if (this.value == '') this.value = 'Name';" />

                                        <textarea name="message" onFocus="if (this.value == 'Message') this.value = '';" onBlur="if (this.value == '') this.value = 'Message';">Mensagem</textarea>

                                        <input class="contact_btn" type="submit" value="Enviar Mensagem" />

                                    </form>

                                </div>

                            </div><!-- //CONTACT FORM -->

                        </div>

                    </div><!-- //ROW -->

                    <div class="row copyright">

                        <div class="col-lg-12 text-center">



                         <p>ArPlus Refrigeração e Climatização - <a href="http://www.roquedigital.com.br/" target="_blank" >Desenvolvido por Roque Digital</a></p>

                        </div>



                    </div><!-- //ROW -->

                </div><!-- //CONTAINER -->

            </footer><!-- //FOOTER -->





            <!-- MAP -->

            <div id="map">

                <a class="map_hide" href="javascript:void(0);" ><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></a>

                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14303.453598818096!2d-48.8490338!3d-26.3309224!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa17a45f796dabc80!2sARPLUS!5e0!3m2!1spt-BR!2sbr!4v1497660696233" ></iframe>

            </div><!-- //MAP -->



        </div>

    </body>

</html>