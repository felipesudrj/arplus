<!DOCTYPE html>

<html lang="en">

<head>



    <meta charset="utf-8">

    <title>Ar Plus Refrigeração e Climatização - Joinville - Santa Catarina - Brasil</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="">



    <link rel="shortcut icon" href="images/favicon.ico">



    <!-- CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="css/flexslider.css" rel="stylesheet" type="text/css" />

    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all" />

    <link href="css/owl.carousel.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet" type="text/css" />

<!--<link href="css/colors/" rel="stylesheet" type="text/css" id="colors" />-->


    <!-- FONTS -->

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500italic,700,500,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">



    <!-- SCRIPTS -->

    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <!--[if IE]><html class="ie" lang="en"> <![endif]-->



    <script src="js/jquery.min.js" type="text/javascript"></script>

    <script src="js/bootstrap.min.js" type="text/javascript"></script>



        <!--Angularjs-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-sanitize.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.min.js"></script>

    <!--end Angularjs-->



    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>

    <script src="js/superfish.min.js" type="text/javascript"></script>

    <script src="js/jquery.flexslider-min.js" type="text/javascript"></script>

    <script src="js/owl.carousel.js"></script>

    <script src="js/animate.js" type="text/javascript"></script>

    <script src="js/myscript.js" type="text/javascript"></script>

    <script src="js/angular/blog.js" type="text/javascript"></script>



</head>

<body ng-app="AppAngular" ng-controller="blogController">



<!-- PRELOADER -->

<img id="preloader" src="images/preloader.gif" alt="" />

<!-- //PRELOADER -->

<div class="preloader_hide">



    <!-- PAGE -->

    <div id="page" class="single_page" >



        <!-- HEADER -->
        <?php require_once('menu.php'); ?>
        <!-- //HEADER -->





        <!-- BREADCRUMBS -->

        <section class="breadcrumbs_block clearfix parallax">

            <div class="container center">

                <h2><b>Blog</b> ArPlus</h2>

                <p>Informações e serviços sobre nossa empresa.</p>

            </div>

        </section><!-- //BREADCRUMBS -->





        <!-- BLOG -->

        <section id="blog">



        <span ng-init="carregarUltimos()"/>

            <!-- CONTAINER -->

            <div class="container">



                <!-- ROW -->

                <div class="row">



                    <!-- BLOG BLOCK -->

                    <div class="blog_block col-lg-9 col-md-9 padbot50">


                        <div class="row text-center" ng-show="sn_carregando">
                            <img src="/images/preloader.gif">
                        </div>

                        <ng-view></ng-view>

                    </div><!-- //BLOG BLOCK -->









                        <!-- SIDEBAR -->

                    <div class="sidebar col-lg-3 col-md-3 padbot50">







                        <!-- POPULAR POSTS WIDGET -->

                        <div class="sidepanel widget_popular_posts">

                            <h3><b>Posts</b> Recentes</h3>



                            <div class="recent_posts_widget clearfix" ng-repeat="(key, value) in arrUltimosPosts">

                                <div class="post_item_img_widget">

                                    <img src="images/blog/1.jpg" alt="" ng-src="{{value.thumbnail_images.full.url}}" />

                                </div>

                                <div class="post_item_content_widget">

                                    <a class="title" href="javascript:void(0);" ng-click="getPost(value.id)" >{{value.title}}.</a>

                                    <ul class="post_item_inf_widget">

                                        <li>{{value.date}}</li>

                                    </ul>

                                </div>

                            </div>



                        </div><!-- //POPULAR POSTS WIDGET -->



                        <hr>





                        <!-- TEXT WIDGET -->

                        <div class="sidepanel widget_text">

                            <h3><b>Nosso</b> Blog</h3>

                            <p>Cras sagittis vitae dui quis posuere. Maecenas ultricies bibendum diam, non tempus ante vestibulum ac. In vitae dignissim ipsum, id iaculis mi. Quisque in purus a augue vehicula consectetur a non dui. Nunc varius, augue vel vehicula rhoncus, purus ipsum venenatis sapien, eget posuere purus sapien eget magna.</p>

                        </div><!-- //TEXT WIDGET -->

                    </div><!-- //SIDEBAR -->

                </div><!-- //ROW -->

            </div><!-- //CONTAINER -->

        </section><!-- //BLOG -->

    </div><!-- //PAGE -->





    <!-- CONTACTS -->

    <section id="contacts">

    </section><!-- //CONTACTS -->



    <!-- FOOTER -->

    <footer>



        <!-- CONTAINER -->

        <div class="container">



            <!-- ROW -->

            <div class="row" data-appear-top-offset="-200" data-animated="fadeInUp">



                <div class="col-lg-4 col-md-4 col-sm-6 padbot30">

                    <h4><b>Posts</b> recentes</h4>

                    <div

                        class="recent_posts_small clearfix"

                        ng-repeat="(key, value) in arrUltimosPosts">



                        <div class="post_item_img_small">

                            <img

                                src="images/blog/1.jpg"

                                alt=""

                                ng-src="{{value.thumbnail_images.full.url}}" />

                        </div>

                        <div class="post_item_content_small">

                            <a

                                class="title"

                                ng-href="<?php echo $current_url;?>/site/blog.php#!post/{{value.id}}"

                                href="blog.html"

                                ng-bind-html="value.title"

                                >



                            </a>

                            <ul class="post_item_inf_small">

                                <li>{{value.date}}</li>

                            </ul>

                        </div>

                    </div>





                </div>



                <div class="col-lg-4 col-md-4 col-sm-6 padbot30 foot_about_block">

                    <h4><b>Sobre</b> nós</h4>

                    <p>Iniciamos as atividades em 2005 com o objetivo de oferecer soluções inteligentes para um mercado cada vez mais exigente e que aspirava por excelência de produtos e serviços. Nossa empresa conta com uma equipe altamente qualificada e comprometida em executar com excelência tudo o que produzimos.</p>

                    <ul class="social">

                        <li><a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a></li>

                        <li><a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>

                        <li><a href="javascript:void(0);" ><i class="fa fa-youtube"></i></a></li>

                        <li><a href="javascript:void(0);" ><i class="map_show fa fa-map-marker"></i></a></li>

                    </ul>

                </div>



                <div class="respond_clear"></div>



                <div class="col-lg-4 col-md-4 padbot30">

                    <h4><b>Fale</b> Conosco</h4>



                    <!-- CONTACT FORM -->

                    <div class="span9 contact_form">

                        <div id="note"></div>

                        <div id="fields">

                            <form id="contact-form-face" class="clearfix" action="#">

                                <input ng-model="ds_nome" type="text" name="name" value="Nome" onFocus="if (this.value == 'Name') this.value = '';" onBlur="if (this.value == '') this.value = 'Name';" />



                                <textarea ng-model="ds_mensagem" name="message" onFocus="if (this.value == 'Message') this.value = '';" onBlur="if (this.value == '') this.value = 'Message';">Mensagem</textarea>





                                <input class="contact_btn" type="button" ng-click="enviarContato()" value="Enviar Mensagem" />

                                <i class="fa fa-spinner fa-spin fa-2x" ng-show="sn_enviando"></i>

                            </form>

                        </div>

                    </div><!-- //CONTACT FORM -->

                </div>

            </div><!-- //ROW -->

            <div class="row copyright">

                <div class="col-lg-12 text-center">



                 <p>ArPlus Refrigeração e Climatização - <a href="http://www.roquedigital.com.br/" target="_blank" >Desenvolvido por Roque Digital</a></p>

                </div>



            </div><!-- //ROW -->

        </div><!-- //CONTAINER -->

    </footer>
    <!-- //FOOTER -->





    <!-- MAP -->

    <div id="map">

        <a class="map_hide" href="javascript:void(0);" ><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></a>

        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14303.453598818096!2d-48.8490338!3d-26.3309224!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa17a45f796dabc80!2sARPLUS!5e0!3m2!1spt-BR!2sbr!4v1497660696233" ></iframe>

    </div><!-- //MAP -->



</div>

</body>

</html>