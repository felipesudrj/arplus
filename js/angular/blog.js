var objAplication = angular.module('AppAngular', ['ngSanitize','ngRoute']);


objAplication.config(function($routeProvider) {
  $routeProvider
  .when("/", {
    templateUrl : "/site/post-all.html",
    controller:"listPostsController",
  })
  .when("/post/:id", {
    templateUrl : "/site/post.html",
    controller:"postController",
  })
});


objAplication.controller('blogController', function($scope,$http,$sce,$filter,$routeParams){


    $scope.arrUltimosPosts = [];
    $scope.carregarUltimos = function()
    {

        $http.get('http://painel.arplus.com.br/api/?json=get_recent_posts&count=3').then(

            function sucesso(objRetorno){

                $scope.arrUltimosPosts = objRetorno.data.posts;



            },

            function erro(objRetorno){



            }

            );
    }










    $scope.sn_enviando = false;

    $scope.enviarContato = function()

    {

        arrDados = {
            ds_nome:'nome',
            ds_mensagem:'telefone'
        }

        $scope.sn_enviando = true;
        $http.post('email.php',arrDados).then(

            function sucesso(objRetorno){
            },

            function erro(objRetorno){
            }).finally(function(){
                $scope.sn_enviando = false;
            });

    }



});

objAplication.controller('listPostsController', function($scope,$http,$sce,$filter,$routeParams){

    $scope.arrPosts = [];
    $scope.nr_page = 1;

    $scope.sn_carregando = false;

    $scope.carregarPosts = function()
    {

        $scope.sn_carregando = true;

        $http.get('http://painel.arplus.com.br/api/?json=get_posts&pages='+$scope.nr_page).then(

            function sucesso(objRetorno){



                angular.forEach(objRetorno.data.posts,function(posts){



                    sn_existe = $filter('filter')($scope.arrPosts,{id:posts.id});



                    if(sn_existe.length==0){

                        $scope.arrPosts.push(posts);

                    }



                });

            },

            function erro(objRetorno){

                console.log('Erro ao carregar posts');

            }

            ).finally(function(){

                $scope.sn_carregando = false;

            });
    }

    $scope.carregarMais = function()
    {

        $scope.nr_page = $scope.nr_page+1;

        $scope.carregarPosts();
    }

});


objAplication.controller('postController', function($scope,$http,$sce,$filter,$routeParams){

    $scope.objPost = null;

    $scope.id = $routeParams.id;
    $scope.$watch('id',function(id){
        if(id){
            $scope.getPost(id);
        }

    });

    $scope.getPost = function(post_id)
    {

        $scope.sn_mostrar_todos = false;

        $scope.sn_carregando = true;

        $http.get('http://painel.arplus.com.br/api/?json=get_post&post_id='+post_id).then(

            function sucesso(objRetorno){

                $scope.objPost = objRetorno.data.post;

            },

            function erro(objRetorno){

                console.log('Erro ao carregar posts');

            }

            ).finally(function(){

                $scope.sn_carregando = false;

            });

    }

});